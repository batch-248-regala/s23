/*
	Activity:
	1. In the s23 folder, create an a1 and an index.html and index.js file inside of it.
	2. Link the script.js file to the index.html file.
	3. Create a trainer object using object literals.
	4. Initialize/add the following trainer object properties:
	- Name (String)
	- Age (Number)
	- Pokemon (Array)
	- Friends (Object with Array values for properties)
	5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	6. Access the trainer object properties using dot and square bracket notation.
	7. Invoke/call the trainer talk object method.
	8. Create a constructor for creating a pokemon with the following properties:
	- Name (Provided as an argument to the contructor)
	- Level (Provided as an argument to the contructor)
	- Health (Create an equation that uses the level property)
	- Attack (Create an equation that uses the level property)
	9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
	10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	11. Create a faint method that will print out a message of targetPokemon has fainted.
	12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
	13. Invoke the tackle method of one pokemon object to see if it works as intended.
	14. Create a git repository named S23.
	15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	16. Add the link in Boodle.
	17. Send an ss of your work
*/

// Creating an object literal "trainer" and adds properties to it
let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemons: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends:{
		Hoenn: ["May", "Max"],
		Kanto: ["Brock", "Misty"],
		},
	};

// Adding the method "talk" to the trainer object that prints the message "Pikachu! I choose you!" when called
trainer.talk = function() {
console.log("Pikachu! I choose you!");
};

// Accessing the properties of the trainer object literal
console.log(trainer);

// Accessing the properties of the trainer object using dot notation
console.log("Results of dot notation: ");
console.log(trainer.name);

// Accessing the properties of the trainer object square bracket notation
console.log("Results of square bracket notation: ");
console.log(trainer["pokemons"]);

// Invoking the "talk" method of the trainer object
console.log("Results of talk method: ");
trainer.talk();

// Creating a constructor "Pokemon" for creating pokemon objects with properties "name", "level", "health", and "attack"
function Pokemon(name, level) {
// Adding the property "name" with the value passed as an argument to the constructor
this.name = name;

// Adding the property "level" with the value passed as an argument to the constructor
this.level = level;

// Adding the property "health" with an equation that calculates the value based on the level property
this.health = level * 2;

// Adding the property "attack" with an equation that calculates the value based on the level property
this.attack = level * 1.5;

// Adding the property "tackle" with an equation that calculates the value based on the level property
this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

// Reducing the target object's health property by subtracting and reassining its value based on the pokemon's attack
// target.health = target.health - this.attack
target.health -= this.attack

console.log(target.name + " health is now reduced to " + target.health);

// If the target's health is less than or equal to 0 we will invoke the faint method, otherwise printout the pokemon's new health

	if(target.health<=0){
		target.faint()
	}
}

// Creating another method called faint
this.faint = function(){
console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon ("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude",8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo",100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);

